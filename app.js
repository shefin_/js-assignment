let updateCalledBy, updateCallerName, newImg;

function hideIndexPage() {
    indexPageDiv = document.getElementById('index-page-div');
    indexPageDiv.style.display = 'none';
}

function showIndexPage() {
    document.title = 'Tourist Places'
    indexPageDiv = document.getElementById('index-page-div');
    indexPageDiv.style.display = 'block';
}

function hideFormPage() {
    formPageDiv = document.getElementById('form-page-div');
    formPageDiv.style.display = 'none';
}

function showFormPage() {
    document.title = 'Add a new Tourist Place'
    formPageDiv = document.getElementById('form-page-div');
    formPageDiv.style.display = 'block';
}

function genIndexPage() {
    const deletableFromForm = document.querySelectorAll('.deletable-form')
    for(element of deletableFromForm) {
        element.remove();
    }

    updateCalledBy = undefined;
    updateCallerName = undefined;

    hideFormPage()
    showIndexPage()
}

function genFormPage() {
    hideIndexPage()
    showFormPage()
}

// document.getElementById('img-file-up').addEventListener("load", createImgUpEvent);

// function createImgUpEvent() {
//     const imgInput = document.getElementById('img-file-up');

//     imgInput.addEventListener("change", imgInputEvent);

//     function imgInputEvent() {
//         const reader = new FileReader();

//         reader.addEventListener("load", () => {
//             newImg = reader.result;
//         });

//         if(this.files) reader.readAsDataURL(this.files[0]);
//         else newImg = "";
//     }
// }

function doDelete() {
    if(!confirm("The Tourist Place Will Be Deleted!!")) return;

    let rowContainer = this.closest('.row-list');
    rowContainer.parentNode.removeChild(rowContainer);
}

function doUpdate() {
    updForm = document.getElementsByClassName('form-tag')[0];
    updateCalledBy = this;
    
    let rowContainer = this.closest('.row-list');
    let divChildren = rowContainer.getElementsByTagName("div");

    for(child of divChildren) {
        if(child.className == 'action')
            continue;

        if(child.className == 'picture') {
            updForm.elements[child.className].value = '';

            let spanElement = document.createElement("span");
            spanElement.classList.add("deletable-form")
            updForm.elements[child.className].nextSibling.nextSibling.insertAdjacentElement("afterend", spanElement);

            let imgTag = child.firstChild;
            let imgDiv = createPictureDiv(imgTag.src);
            imgDiv.style.cssFloat = "left";
            imgDiv.classList.add("deletable-form", "img-preview");
            updForm.elements[child.className].nextSibling.nextSibling.insertAdjacentElement("afterend", imgDiv);

            let labelElement = document.createElement("label");
            labelElement.textContent = "Old Picture:"
            labelElement.classList.add("deletable-form")
            updForm.elements[child.className].nextSibling.nextSibling.insertAdjacentElement("afterend", labelElement);

            continue;
        }

        updForm.elements[child.className].value = child.textContent.trim();

        if(child.className == 'name') updateCallerName = child.textContent.trim();
    }

    genFormPage();
}

function createNameDiv(name) {
    existingNames = document.getElementsByClassName('name');
    for(let i = 0; i < existingNames.length; i++) {
        let placeName = existingNames[i].innerText;
        if(placeName == name && placeName != updateCallerName)
            return null;
    }

    divElement = document.createElement("div");
    divElement.className = "name";
    divElement.innerText = name;
    return divElement;
}

function createAddressDiv(address) {
    divElement = document.createElement("div");
    divElement.className = "address";
    divElement.innerText = address;
    return divElement;
}

function createRatingDiv(rating) {
    divElement = document.createElement("div");
    divElement.className = "rating";
    divElement.innerText = rating;
    return divElement;
}

function createPictureDiv(img) {
    divElement = document.createElement("div");
    divElement.className = "picture";

    imgElement = document.createElement("img");
    // console.log(img);
    imgElement.src = img;
    imgElement.height = "200";
    imgElement.width = "150";

    divElement.appendChild(imgElement);
    return divElement;
}

function createButtonsDiv() {
    divElement = document.createElement("div");
    divElement.className = "action";

    spanElement = document.createElement("span");

    buttonUpdate = document.createElement("button");
    buttonUpdate.type = "button";
    buttonUpdate.className = "btn-blue";
    buttonUpdate.innerText = "Update";
    buttonUpdate.addEventListener("click", doUpdate);

    buttonDelete = document.createElement("button");
    buttonDelete.type = "button";
    buttonDelete.className = "btn-red";
    buttonDelete.innerText = "Delete";
    buttonDelete.addEventListener("click", doDelete);

    spanElement.appendChild(buttonUpdate);
    spanElement.appendChild(document.createTextNode(" "));
    spanElement.appendChild(buttonDelete);

    divElement.appendChild(spanElement);
    return divElement;
}

function getImageForm(fileField) {
    if(fileField.files.length == 0)
        return null;

    const dir = "upload/";
    return dir+fileField.files[0].name;
}

function submitForm(event) {
    event.preventDefault();

    formNew = document.getElementsByClassName('form-tag')[0];

    const name = formNew.elements["name"].value;
    const address = formNew.elements["address"].value;
    const rating = formNew.elements["rating"].value;
    const type = formNew.elements["type"].value;
    let img = getImageForm(formNew.elements["picture"]);
    // const img = newImg;

    if(name) {
        formNew.elements["name"].nextSibling.nextSibling.innerHTML = "";
        formNew.elements["name"].style.borderColor = "black";
    }

    if(address) {
        formNew.elements["address"].nextSibling.nextSibling.innerHTML = "";
        formNew.elements["address"].style.borderColor = "black";
    }

    if(rating) {
        formNew.elements["rating"].nextSibling.nextSibling.innerHTML = "";
        formNew.elements["rating"].style.borderColor = "black";
    }

    if(type) {
        formNew.elements["type"].nextSibling.nextSibling.innerHTML = "";
        formNew.elements["type"].style.borderColor = "black";
    }

    if(img || updateCalledBy) {
        formNew.elements["picture"].nextSibling.nextSibling.innerHTML= "";
        formNew.elements["picture"].style.borderColor = "black";
    }

    if(!name || !address || !rating || !type || (!img && !updateCalledBy)) {

        if(!name) {
            nextSpan = formNew.elements["name"].nextSibling.nextSibling;
            nextSpan.innerHTML = "<b> *Required </b>";
            nextSpan.style.color = "red";

            formNew.elements["name"].style.borderColor = "red";
        }

        if(!address) {
            nextSpan = formNew.elements["address"].nextSibling.nextSibling;
            nextSpan.innerHTML = "<b> *Required </b>";
            nextSpan.style.color = "red";

            formNew.elements["address"].style.borderColor = "red";
        }

        if(!rating) {
            nextSpan = formNew.elements["rating"].nextSibling.nextSibling;
            nextSpan.innerHTML = "<b> *Required </b>";
            nextSpan.style.color = "red";

            formNew.elements["rating"].style.borderColor = "red";
        }

        if(!type) {
            nextSpan = formNew.elements["type"].nextSibling.nextSibling;
            nextSpan.innerHTML = "<b> *Required </b>";
            nextSpan.style.color = "red";

            formNew.elements["type"].style.borderColor = "red";
        }

        if(!img && !updateCalledBy) {
            nextSpan = formNew.elements["picture"].nextSibling.nextSibling;
            nextSpan.innerHTML = "<b> *Required </b>";
            nextSpan.style.color = "red";

            formNew.elements["picture"].style.borderColor = "red";
        }

        alert("Please Fill Up All The Fields!");
        return;
    }
    
    if(Number(rating) < 1 || Number(rating) > 5) {
        alert("Rating Must Be In [1,5] Range");
        return;
    }

    divRow = document.createElement("div");
    divRow.className = "row-list";

    nameDiv = createNameDiv(name);
    if(nameDiv == null) {
        alert("Tourist Places' Names Should Be Unique !");
        return;
    }
    
    if(updateCalledBy) {
        updateForm();
        return;
    }

    divRow.appendChild(nameDiv);
    divRow.appendChild(createAddressDiv(address));
    divRow.appendChild(createRatingDiv(rating));
    divRow.appendChild(createPictureDiv(img));
    divRow.appendChild(createButtonsDiv());

    divContainer = document.getElementsByClassName('container-list')[0];
    divContainer.appendChild(divRow);

    genIndexPage();
}

function updateForm() {
    formNew = document.getElementsByClassName('form-tag')[0];

    const name = formNew.elements["name"].value;
    const address = formNew.elements["address"].value;
    const rating = formNew.elements["rating"].value;
    const type = formNew.elements["type"].value;
    let img = getImageForm(formNew.elements["picture"]);
    // const img = newImg;

    let elem = updateCalledBy;
    let rowContainer = elem.closest('.row-list');
    let divChildren = rowContainer.getElementsByTagName("div");
    let imgTag = rowContainer.getElementsByTagName('img')[0];

    if(!img) img = imgTag.src;

    function applyUpdate(elem) {

        for(child of divChildren) {
            if(child.className == 'action')
                continue;

            if(child.className == 'picture') {
                img_child = child.children[0];
                img_child.src = img;
                continue;
            }

            child.textContent = updForm.elements[child.className].value;
        }
    }

    applyUpdate(updateCalledBy);
    genIndexPage();
}

function sortContents(criteria, ascending=true) {
    rows = document.getElementsByClassName('row-list');
    parentElement = rows[0].parentElement;

    araRows = Array.from(rows);
    araRows.shift(); // 0th row is the header.

    function comp(item1, item2) {
        val1 = item1.getElementsByClassName(criteria)[0].innerText;
        val2 = item2.getElementsByClassName(criteria)[0].innerText;

        if(val1 > val2) {
            return ascending? 1 : -1;
        }
        else if(val1 == val2)
            return 0;
        else {
            return ascending? -1 : 1;
        }
    }

    araRows.sort(comp);

    for(row of araRows) {
        parentElement.appendChild(row);
    }   
}

function sortItems(elem) {
    let arrowType = elem.className.trim();
    let criteria = elem.parentElement.textContent.trim().toLowerCase();
    
    if(arrowType == 'down-arrow') sortContents(criteria, true);
    else sortContents(criteria, false);
}

function searchMatch(searchText, placeName) {
    if(!searchText) return true;

    searchText = searchText.toLowerCase();
    placeName = placeName.toLowerCase();

    return placeName.indexOf(searchText) != -1;
}

function performSearch(e) {
    searchText = e.target.value;

    places = document.getElementsByClassName('row-list');
    let odd = 0;

    for(i = 1; i < places.length; i++) { // i=0 is the header
        placeName = places[i].getElementsByClassName('name')[0].innerText;

        if(!searchMatch(searchText, placeName)) {
            places[i].style.display = 'none';
        }
        else {
            places[i].style.display = 'grid';

            // divChildren = places[i].getElementsByTagName("div");

            // if(odd == 1) {
            //     for(item of divChildren) item.style.backgroundColor = "#ddd";
            // }
            // else {
            //     for(item of divChildren) item.style.backgroundColor = "#fff";
            // }

            // odd ^= 1;
        }
    }
}

window.addEventListener("load", searchEvent);

function searchEvent() {
    searchBox = document.getElementById("search-text-box");
    searchBox.addEventListener("input", performSearch);
}